<?php
// HTTP
define('HTTP_SERVER', 'http://beerka-new.local/admin/');
define('HTTP_CATALOG', 'http://beerka-new.local/');

// HTTPS
define('HTTPS_SERVER', 'http://beerka-new.local/admin/');
define('HTTPS_CATALOG', 'http://beerka-new.local/');

// DIR
define('DIR_APPLICATION', '/var/www/beerka-new/admin/');
define('DIR_SYSTEM', '/var/www/beerka-new/system/');
define('DIR_IMAGE', '/var/www/beerka-new/image/');
define('DIR_LANGUAGE', '/var/www/beerka-new/admin/language/');
define('DIR_TEMPLATE', '/var/www/beerka-new/admin/view/template/');
define('DIR_CONFIG', '/var/www/beerka-new/system/config/');
define('DIR_CACHE', '/var/www/beerka-new/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/beerka-new/system/storage/download/');
define('DIR_LOGS', '/var/www/beerka-new/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/beerka-new/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/beerka-new/system/storage/upload/');
define('DIR_CATALOG', '/var/www/beerka-new/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'denis');
define('DB_PASSWORD', 'password');
define('DB_DATABASE', 'beerka_new');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
