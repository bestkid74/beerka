<div class="categories">
  <h5>Категории товаров</h5>
  <?php foreach ($categories as $category) { ?>
    <ul>
      <li>
        <a class="toggle-a" style="<?php if (!$category['children']) { echo "display: none";}?>" href="#"></a>
        <a class="category-name-a<?php if ($category['category_id'] == $category_id) { echo ' active';} ?>" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
        <?php if ($category['children']) { ?>
          <ul>
            <?php foreach ($category['children'] as $child) { ?>
              <li>
                <a style="<?php if (!$child['children2']) { echo "display: none";}?>" class="<?php if ($child['children2']) { echo "toggle-a";} ?>" href="#"></a>
                <a href="<?php echo $child['href']; ?>" class="sub-category-name-a <?php if ($child['category_id'] == $child_id) { echo "active";} ?>"><?php echo $child['name']; ?></a>
                <ul>
                  <?php foreach ($child['children2'] as $child_lv3) { ?>
                    <li>
                      <a href="<?php echo $child_lv3['href']; ?>" class="<?php if ($child2_id == $child_lv3['category_id']) { echo "active";} ?>"><?php echo $child_lv3['name']; ?></a>
                    </li>
                  <?php } ?>
                </ul>
              </li>
            <?php } ?>
          </ul>
        <?php } ?>
      </li>
    </ul>
  <?php } ?>
</div>

