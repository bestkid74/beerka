<h3><?php echo $heading_title; ?></h3>
<div class="row">
  <div class="product-layout col-sm-12">
    <div class="products grid">
      <?php foreach ($products as $product) { ?>
        <div class="product-thumb transition">
          <div class="image">
            <a href="<?php echo $product['href']; ?>">
              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
            </a>

              <?php if ($product['special']) { ?>
                <span class="price-discount">-<?php echo round((floatval($product['price']) - floatval($product['special'])) / floatval($product['price']) * 100); ?>%</span>
              <?php } ?>

              <?php if ($product['rating']) { ?>
                <div class="rating">
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <span class="rating-value"><?php echo $product['rating']; ?></span>
                </div>
              <?php } ?>

            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i></button>
          </div>
          <div class="caption">
            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>

            <div class="action-box">
              <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');">
                <?php echo $button_cart; ?>
                <img src="catalog/view/theme/beerka/image/add-to-cart-icon.svg" alt="" />
              </button>
                <?php if ($product['price']) { ?>
                  <p class="price">
                    <?php if (!$product['special']) { ?>
                      <?php echo $product['price']; ?>
                    <?php } else { ?>
                      <?php echo $product['price']; ?>
                      <span class="price-new"><?php echo $product['special']; ?></span>
                    <?php } ?>
                  </p>
                <?php } ?>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
