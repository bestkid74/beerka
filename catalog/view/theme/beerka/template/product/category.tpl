<?php echo $header; ?>
<div class="container">
  <div class="category__top">
    <ul class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>

    <div class="category__top_action">
      <div class="category__top_action_filters-mobile">
        Фильтры поиска:<a href="#"><img src="catalog/view/theme/beerka/image/filter-icon.svg" alt="" /></a>
      </div>
      <div class="category__top_action_sort">
        <label for="input-sort"><?php echo $text_sort; ?></label>
        <select id="input-sort" onchange="location = this.value;">
          <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
          <?php } ?>
        </select>
      </div>
      <div class="category__top_action_grid">
        <button type="button" id="list-view" class="btn-list" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
        <button type="button" id="grid-view" class="btn-grid" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
      </div>
    </div>
  </div>
  <h1 class="category__title"><?php echo $heading_title; ?></h1>

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if ($categories) { ?>
        <div class="category__subcats-thumbs">
          <ul>
            <?php foreach ($categories as $category) { ?>
              <li>
                <a href="<?php echo $category['href']; ?>">
                  <?php if ($category['thumb']) { ?>
                  <img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" />
                  <?php } ?>
                  <span><?php echo $category['name']; ?></span>
                </a>
              </li>
            <?php } ?>
          </ul>
        </div>
      <?php } ?>

      <?php if ($products) { ?>
      <div class="row">
        <div class="product-layout category col-sm-12">
          <div class="products grid">
            <?php foreach ($products as $product) { ?>
              <div class="product-thumb transition">
                <div class="image">
                  <a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                  </a>

                    <?php if ($product['special']) { ?>
                      <span class="price-discount">-<?php echo round((floatval($product['price']) - floatval($product['special'])) / floatval($product['price']) * 100); ?>%</span>
                    <?php } ?>

                    <?php if ($product['rating']) { ?>
                      <div class="rating">
                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                        <span class="rating-value"><?php echo $product['rating']; ?></span>
                      </div>
                    <?php } ?>

                  <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i></button>
                </div>
                <div class="caption">
                  <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>

                  <?php if ($product['rating']) { ?>
                    <div class="rating">
                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                      <span class="rating-value"><?php echo $product['rating']; ?></span>
                    </div>
                  <?php } ?>

                  <p class="product-description"><?php echo $product['description']; ?></p>

                  <div class="action-box">
                    <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');">
                      <?php echo $button_cart; ?>
                      <img src="catalog/view/theme/beerka/image/add-to-cart-icon.svg" alt="" />
                    </button>
                      <?php if ($product['price']) { ?>
                        <p class="price">
                          <?php if (!$product['special']) { ?>
                            <?php echo $product['price']; ?>
                          <?php } else { ?>
                            <?php echo $product['price']; ?>
                            <span class="price-new"><?php echo $product['special']; ?></span>
                          <?php } ?>
                        </p>
                      <?php } ?>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
