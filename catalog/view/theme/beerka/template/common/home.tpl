<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?></div>
    <?php echo $column_right; ?></div>
  <div class="row">
    <div class="col-sm-12">
        <?php echo $content_bottom; ?>
    </div>
  </div>
</div>
<section class="features">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="features__content">
          <div class="features__content_item">
            <div class="img-wrapper">
              <img src="catalog/view/theme/beerka/image/f_icon_1.svg" alt="" />
            </div>
            <div class="features__content_item_text">
              <h5>МГНОВЕННАЯ ОТПРАВКА</h5>
              <p>При оплате заказа товаров до 15:00,<br>отправка в тот же день</p>
            </div>
          </div>
          <div class="features__content_item">
            <div class="img-wrapper">
              <img src="catalog/view/theme/beerka/image/f_icon_2.svg" alt="" />
            </div>
            <div class="features__content_item_text">
              <h5>ЗАЩИЩЕННАЯ ОПЛАТА</h5>
              <p>Возможность оплатить товары<br>сразу через сайт</p>
            </div>
          </div>
          <div class="features__content_item">
            <div class="img-wrapper">
              <img src="catalog/view/theme/beerka/image/f_icon_3.svg" alt="" />
            </div>
            <div class="features__content_item_text">
              <h5>ПРОГРАММА ЛОЯЛЬНОСТИ</h5>
              <p>Для владельцев карт лояльности –<br>постоянные бонусы и скидки</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php echo $footer; ?>
