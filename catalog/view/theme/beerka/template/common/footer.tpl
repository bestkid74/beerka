<footer>
  <div class="container">
    <div class="footer__contacts">
      <div class="footer__contacts_logo">
        <?php if ($logo) { ?>
          <?php if ($home == $og_url) { ?>
            <img src="<?php echo $logo; ?>" alt="Beerka" class="footer-logo" />
            <a href="<?php echo $instagram; ?>">НАШ INSTAGRAM <img src="catalog/view/theme/beerka/image/insta-icon.svg" class="insta" alt="insta" /></a>
          <?php } else { ?>
            <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" alt="Beerka" class="footer-logo" /></a>
            <a href="<?php echo $instagram; ?>">НАШ INSTAGRAM <img src="catalog/view/theme/beerka/image/insta-icon.svg" class="insta" alt="insta" /></a>
          <?php } ?>
        <?php } ?>
      </div>

      <div class="footer__contacts_wrapper">
        <div class="footer__contacts_wrapper_phones">
          <ul>
            <li><a href="tel:<?php echo str_replace(' ','', $telephone); ?>"><?php echo $telephone; ?></a></li>
            <li><a href="tel:+38<?php echo str_replace(' ','', $telephone_2); ?>"><?php echo $telephone_2; ?></a></li>
            <li>
              <a href="tel:+38<?php echo str_replace(' ','', $telephone_3); ?>">
                <img src="catalog/view/theme/beerka/image/telegram-logo.svg" alt="" />
                <img src="catalog/view/theme/beerka/image/viber-logo.svg" alt="" />&nbsp;<?php echo $telephone_3; ?>
              </a>
            </li>
            <li><a href="mailto:<?php echo str_replace(' ','', $email); ?>"><i class="fa fa-envelope-o"></i>&nbsp;<?php echo $email; ?></a></li>
          </ul>
        </div>
        <div class="footer__contacts_wrapper_address">
          <div class="footer__contacts_wrapper_address_address">
            <?php echo $address; ?>
          </div>
          <div class="footer__contacts_wrapper_address_open">
            <h5><?php echo $text_open; ?></h5>
            <?php echo $open; ?>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="footer__content">
      <?php if ($informations) { ?>
        <div class="footer__content_col">
          <h5><?php echo $text_information; ?></h5>
          <ul class="list-unstyled">
            <?php foreach ($informations as $information) { ?>
              <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      <?php } ?>

      <div class="footer__content_col">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>

      <div class="footer__content_col">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
  </div>

  <div class="footer__copyright">
      <?php echo $powered; ?>
  </div>
</footer>

<a id="button-top">
  <img src="catalog/view/theme/beerka/image/arr_top.svg" alt="" />
</a>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>
