<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title;  ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
  <link href="catalog/view/theme/beerka/stylesheet/stylesheet.css?ver=<?php echo mt_rand(0, mt_getrandmax())/mt_getrandmax(); ?>" rel="stylesheet">
<script src="catalog/view/theme/beerka/js/common.js?ver=<?php echo mt_rand(0, mt_getrandmax())/mt_getrandmax(); ?>" type="text/javascript"></script>
<script src="catalog/view/theme/beerka/js/scripts.js?ver=<?php echo mt_rand(0, mt_getrandmax())/mt_getrandmax(); ?>" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">

<header>
  <div class="container">
    <div class="header__content">
      <div class="header__content_logo">
        <?php if ($logo) { ?>
          <?php if ($home == $og_url) { ?>
          <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
          <?php } else { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
          <?php } ?>
        <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
        <?php } ?>
      </div>
      <div class="header__content_wrapper">
        <div class="header__content_wrapper_search hidden-xs">
            <?php echo $search; ?>
        </div>
        <div class="header__content_wrapper_menu">
          <ul>
            <li class="dropdown">
              <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown">
                <span class="hidden-xs"><?php echo $text_account; ?></span>
                <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M15.7496 12.2825C17.5872 10.7843 18.6209 8.47947 18.2763 5.8289C17.8169 2.8326 15.2901 0.412515 12.304 0.0667884C8.05441 -0.509423 4.60882 2.71736 4.60882 6.86608C4.60882 9.05569 5.6425 11.0148 7.13559 12.2825C3.23059 13.7806 0.474119 17.1226 0.0147076 21.7323C-0.100145 22.4238 0.474119 23 1.16324 23C1.7375 23 2.19691 22.539 2.31177 21.9628C2.77118 16.7769 6.67618 13.7806 11.5 13.7806C16.209 13.7806 20.2288 16.7769 20.6882 21.9628C20.6882 22.539 21.2625 23 21.8368 23C22.5259 23 23.1001 22.4238 22.9853 21.7323C22.411 17.1226 19.6546 13.6654 15.7496 12.2825ZM11.5 11.4758C8.97324 11.4758 6.90588 9.40141 6.90588 6.86608C6.90588 4.33075 8.97324 2.25639 11.5 2.25639C14.0268 2.25639 16.0941 4.33075 16.0941 6.86608C16.0941 9.40141 14.0268 11.4758 11.5 11.4758Z" fill="#8D1540"/>
                </svg>
              </a>
              <ul class="dropdown-menu dropdown-menu-right">
                  <?php if ($logged) { ?>
                    <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                    <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                    <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                    <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                    <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                  <?php } else { ?>
                    <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                    <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                  <?php } ?>
              </ul>
            </li>
            <li>
                <?php echo $cart; ?>
            </li>
            <li class="dropdown">
              <div class="hamburger dropdown-toggle" id="hamburger-6" data-toggle="dropdown">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
              </div>
              <ul class="dropdown-menu dropdown-menu-right">
                <?php if ($home != $og_url) { ?>
                  <li><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
                <?php } ?>
                <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                <li><a href="#">Beerka club</a></li>
                  <?php if ($informations) { ?>
                    <?php foreach ($informations as $information) { ?>
                      <?php if ($information['id'] == 4) { ?>
                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                <li><a href="<?php echo $contact; ?>"><?php echo $text_contacts; ?></a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="header__search-mobile hidden-sm hidden-md hidden-lg">
        <?php echo $search; ?>
    </div>
  </div>
</header>

<?php if ($categories) { ?>
  <div class="main-menu">
    <div class="container">
      <div class="main-menu__list">
        <ul>
          <?php foreach ($categories as $category) { ?>
            <li>
              <a href="<?php echo $category['href']; ?>">
                <div class="mobile-img hidden-sm hidden-md hidden-lg">
                  <img src="catalog/view/theme/beerka/image/<?php echo $category['id']; ?>.svg" alt="" />
                </div>
                <?php echo $category['name']; ?>
              </a>
            </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
<?php } ?>
