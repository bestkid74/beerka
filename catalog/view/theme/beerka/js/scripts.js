$(document).ready(function () {

  var btn = $('#button-top');

  $(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
      btn.addClass('show-btn');
    } else {
      btn.removeClass('show-btn');
    }
  });

  btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '800');
  });

  // menu categories
  $('.category-name-a.active').parent().children('.toggle-a').addClass('open');
  $('.sub-category-name-a.active').parent().children('.toggle-a').addClass('open');
  $('.toggle-a').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('open');
  });

  // breadcrumbs
  $('.breadcrumb>li:first-child a').html("<img src='catalog/view/theme/beerka/image/home_btn.svg' alt='' />");
});
